# Instrcutions to install 

1) If running windows get git terminal installed 
2) git clone https://gitlab.com/araa/tagid_erm
3) cd 'tagid_erm'
4) Install pipenv on windows and python3 on windows (https://github.com/LambdaSchool/CS-Wiki/wiki/Installing-Python-3-and-pipenv)
5) cd into tagid_erm (cd tagid_erm)
6) Run 'pipenv shell'
7) Install dependencies (pipenv install)
8) Create gspread_secret.json file on google, copy paste to notebook directory 


# Creating table on db 

1) cd apps 
2) open table_creation.py, fix db config details 
3) Run python3 table_creation.py 
4) Table shld be created on db 

# Running notebooks 

1) cd 'tagid_erm'
2) make sure pipenv is activated from steps above if not run pipenv shell 
3) run 'jupyter-notebook', this should open browser with all the files 
4) Use common sense from here onwards! 
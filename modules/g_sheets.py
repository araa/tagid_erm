import gspread 
import os 
from oauth2client.service_account import ServiceAccountCredentials 
import json



## Simple function that intializes google sheets and passes sheet object to be used, returns exception if errors
def initalize_google_api(filename):
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    # get environment check if its on heroku or on mac (dev vs production)
    secret = "gspread_secret.json"
    try:
        ## local envirnoment
       
        creds = ServiceAccountCredentials.from_json_keyfile_name(secret, scope)
       
        client = gspread.authorize(creds)
        sheet = client.open(filename)
       # worksheet = sheet.worksheet(sheetname)    
        print("Google Sheets Credentials Sucessfully Initialized!")  
        return sheet
    except Exception as e:
        print("Exception occured at initalize_google_api() : %s"%e)
        return e





def cred_init():
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    secret = "gspread_secret.json"

    try:
       
        creds = ServiceAccountCredentials.from_json_keyfile_name(secret, scope)
        client = gspread.authorize(creds)
        return client
       # worksheet = sheet.worksheet(sheetname)    
        print("Google Sheets Credentials Sucessfully Initialized!")  
        return client
    except Exception as e:
        print("Exception occured at initalize_google_api() : %s"%e)
        return e



def check_sheet(client, filename):
    try:
        # check if it exists first 
        sheet = client.open(filename)
    except Exception as e:
        # this means file doesn't exist 
        sheet = client.create(filename)
        sheet.share('akshay@tagid.co.in', perm_type='user', role='writer')

    return sheet 






def create_and_fill_cell(df, start, sheet, title):
    def get_size(df):
        size = df.shape
        actual_size = [0,0]
        actual_size[0] = size[0] + 1
        actual_size[1] = size[1] + 1
        return actual_size 


    def get_relative_row_col(start, cell):
        row = cell.row
        col = cell.col 
        row = row - start[0]
        col = col - start[1]
        return row, col 

    size = get_size(df)
    df = df.fillna('-')
    # create a sheet 
    start_point = start 
    end_point = [a + b -1 for a, b in zip(start, size)] 
    cell_list = sheet.range(start_point[0], start_point[1], end_point[0], end_point[1])
    for cell in cell_list:
        cell.value=0
        rel_row, rel_col = get_relative_row_col(start_point, cell)
        # this adds the column indexs 
        if rel_row == 0:
            if rel_col == 0:
                cell.value = df.index.name 
            else:
                # These are column names which are always str
                cell.value = str(df.columns[rel_col-1])
        else:
            if rel_col == 0:
                # These are always names too usually 
                cell.value = str(df.index[rel_row-1])
            else:
                #print((pnl[df.columns[rel_col-1]])[0])
                try:
                    cell.value = float((df[df.columns[rel_col-1]])[rel_row-1])
                except Exception as e:
                    cell.value = str((df[df.columns[rel_col-1]])[rel_row-1])
                
 
    
    sheet.update_cells(cell_list)

    ## Now we minus -1 from the start point[0] and write a name
    
    # start_point[0] = start_point[0] - 1 
    
    # sheet.update_cell(start_point[0], start_point[1], title)
    # return row number 
    return end_point
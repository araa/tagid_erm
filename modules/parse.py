import pandas as pd 
import numpy as np
import os 
from datetime import datetime
from datetime import timedelta
import g_sheets
from gspread_dataframe import get_as_dataframe, set_with_dataframe
import json 



################ Functions for reading csv, calculating dates, and importing trade/pnl/balance files as pandas tables ######################

# Get tomorrows date given todays date in string format 
def get_tomz(today):
    python_today = datetime.strptime(today, "%Y-%m-%d")
    python_tomz = python_today + timedelta(days=1)
    tomz = datetime.strftime(python_tomz, "%Y-%m-%d")
    return tomz 


# Simple function to read csv and return pandas df 
def read_csv(filename, **params):
    try:
        df = pd.read_csv(filename, **params)
        return df 
    except Exception as err:
        print("Exception occured while trying to read csv file: %s"%err)
        return err

# Functions to read pnl and return pandas df 
def read_pnl(filename):
    # Read the csv 
    pnl = read_csv(filename, thousands=',', index_col=0)
    # add name to the index 
    pnl.index.names = ['Coin']
    # Fill na with zero's 
    pnl = pnl.fillna(0)
    return pnl 

# Function to read balances csv and return pandas df 
def read_balance(filename):
    balance = read_csv(filename)
    # Rename the columns 
    balance = balance.rename(columns={'Unnamed: 0': 'Coin'})
    balance = balance.rename(columns={'Unnamed: 1': 'Free/Total'})
    # change index to coin 
    balance.set_index(['Coin'])
    # fill na's with '-'
    balance["Free/Total"] = balance["Free/Total"].fillna('-')
    # Fill na's with '-'
    balance["Coin"] = balance["Coin"].fillna('-')
    return balance 

# Function to read trades csv and return pandas df 
def read_trades(filename):
    # Read the csv 
    trades = read_csv(filename)
    return trades 

# Function that combines all trade files in a directory that belongs to the same account 
def get_trades_from_dir(file_dir, date, account):
    file_dir = file_dir + 'Trades'
    filesindir = os.listdir(file_dir)
    today_trade_files = []
    for file in filesindir:
        if 'Trades' in file:
            if date in file: 
                today_trade_files.append(file)
    all_trades = pd.DataFrame()
    try:
        for files in today_trade_files:
            full_path = "%s/%s"%(file_dir, files)
            trades = read_trades(full_path)
            if trades['account'][0] == account:
                print("Trade file being used: %s"%(files))
                all_trades =  pd.concat([trades, all_trades], sort=False)
        all_trades = all_trades.set_index("trade_time")
        all_trades = all_trades.sort_values(by=['trade_time'])
        return all_trades
    except Exception as e:
        print(e)
        return 


def sorted_dir(folder):
    def getmtime(name):
        path = os.path.join(folder, name)
        return os.path.getmtime(path)

    return sorted(os.listdir(folder), key=getmtime, reverse=True)
# Function that gets the balance and pnl for todays date,account and file direcotry  
def get_pnl_balance_from_dir(file_dir, date, account):
    pnl_files = []
    balance_files = []
    file_dir = file_dir + 'PnL'
    # get tomz date since these files are created at 0 utc the next day
    #tomorrow_date = get_tomz(date)
    # get all files in the dor
    fileisindir = sorted_dir(file_dir)
    # read the balance and pnl 
    for file in fileisindir:
        pathtofile = "%s/%s"%(file_dir, file)
        if date in file:
            if account in file:
                if 'BAL' in file:
                    balance_files.append(file)
                if 'PNL' in file:
                    pnl_files.append(file)

    latest_pnl = pnl_files[0]
    latest_balance = balance_files[0]


    # Use the first 0 utc files 
    print("Balance file choosen: %s"%(latest_balance))
    print("Pnl file choosen: %s"%latest_pnl)
    
    balance = read_balance("%s/%s"%(file_dir, latest_balance))
    pnl = read_pnl("%s/%s"%(file_dir, latest_pnl))
    return pnl, balance 


# Function that takes all combines trades as input, and splits it based on base_currency and returns a dictionary of df based on each base_currency 
def get_unique_trades(all_trades):
    unique_bases = list(all_trades['base_currency'].unique())
    trade_dict = {}
    for item in unique_bases:
       trade_dict[item] = all_trades.loc[all_trades['base_currency'] == item]
    return trade_dict 



# # # # # # # # # # # # # # # # # # # # # # # #  This is the only function called from a notebook# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Reads all as data frames given date, file_dir and account 
def read_all_as_tables(file_dir, date, account):
    pnl = None 
    balance = None 
    all_trades = None
    trade_dict = None 
    try:
        pnl, balance = get_pnl_balance_from_dir(file_dir, date, account)
        all_trades = get_trades_from_dir(file_dir, date, account)
        trade_dict = get_unique_trades(all_trades)
    except Exception as e:
        print(e)

    return pnl, balance, all_trades, trade_dict 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

###########################################################################################################################

# Gets all xrates
def get_xrates(trades, pnl, primary_currency):
    xrates = {}
    primary_cur_rate_tag = "%s_Rate"%primary_currency
    primary_currency_list = list(trades['quote_currency'].unique())
    trade_currency_list = list(trades['base_currency'].unique())
    pnl = pnl.drop(['Total'])
    # set the USDT/USDT rate to 1 in this case 
    xrates["%s/%s"%(primary_currency, primary_currency)] = 1

    #now add all the usdt rates from pnl 
    for index, row in pnl.iterrows():
        pair = "%s/%s"%(index,primary_currency )
        xrates[pair] = row['USDT_Rate']
    
 
    # get all other rates based on the above rates  
    for item in trade_currency_list:
        for quote in primary_currency_list:
            pair = "%s/%s"%(item, quote)
            if quote != primary_currency:
                xrates[pair] = xrates.get("%s/%s"%(item, primary_currency))  /    xrates.get("%s/%s"%(quote, primary_currency))
    return xrates

# Gets xrates as a pandas df 
def get_xrates_table(xrates, date):
    index_keys = list(xrates.keys())
    unique_pairs = []
    for item in index_keys:
        split = item.split('/')
        if split[0] == split[1]:
            pass
        else:
            unique_pairs.append(item)
    unique_pairs.sort()
    unique_pairs

    corresponding_prices = []
    for item in unique_pairs:
        corresponding_prices.append(xrates.get(item, 1))
    data = {date: corresponding_prices}
    xrates = pd.DataFrame.from_dict(data, orient='index', columns=unique_pairs)
    xrates.index.name = 'Date'
    return xrates 


def parse_trades(trades, all_rates, primary_currency, new_version): 

    def get_local_price(row):
        pair = row['pair']
        return all_rates.get(pair, 0)

    def pnl_local_currency_calc(row):
        current_value = (row['local_price']-row['fill_price'])*row['fill_size']
        if row['side'] == "SELL":
            current_value = current_value * -1 
        return current_value

    def pnl_primary_currency_calc(row):
        pair = row['quote_currency'] + '/' + primary_currency
        return row['pnl_local']*all_rates.get(pair, 1)

    def theo_hedgecost_calc(row):
        pair = row['quote_currency'] + '/' + primary_currency
        current_value = (row['synth_price']- all_rates.get(pair, 1)*row['fill_price'])*row['fill_size']
        if row['side'] == "SELL":
            current_value = current_value * -1 
        return current_value

    def hedge_pnl_calc(row):
        pair = row['base_currency'] + '/' + primary_currency
        current_value = (all_rates.get(pair, 1)-row['synth_price'])*row['fill_size']
        if row['side'] == "SELL":
            current_value = current_value * -1 
        return current_value

    def get_level(row):
        if new_version:
            try:
                info = json.loads(str(row['info']).replace("'", "\""))
                level = info['LEVEL']
                try:
                    level = int(level)
                except Exception as e:
                    level = 0 
                return level 
            except Exception as e:
                info = str(row['info'])
                level = info.split(",")[1].split(":")[1]
                if level == 'UNK':
                    level = 0 
                return int(level)
        else:    
            info = str(row['info'])
            info = info.split(',')[0]
            info = info.split(':')[1]
            return int(info)
        
    def get_fees(row):

        if new_version:
            try:
                info = json.loads(str(row['info']).replace("'", "\""))
                fees = info['FEES']
                try:
                    fees = float(fees)
                except Exception as e:
                    fees = 0 
                return fees 
            except Exception as e:
                info = str(row['info'])
                fees = info.split(",")[6].split(":")[1]
                return float(fees)
        else:

            info = str(row['info'])
            if 'fees' in info:
                info = info.split(':')[-1]
                info = info.split(']')[0]
                return float(info)
            else:
                return 0 
        
    def maket_taker_calc(row):
        if new_version:
            try:
                info = json.loads(str(row['info']).replace("'", "\""))
                intention = info['INTENTION']
                if intention == "AGGRESSIVE":
                    return "Taker"
                else:
                    return "Maker"
            except Exception as e:
                info = str(row['info'])
                intention  = info.split(",")[4].split(":")[1]
                if intention == "AGGRESSIVE":
                    return "Taker"
                else:
                    return "Maker"
        else:
            info = str(row['info'])
            if "AGGRESSIVE" in info:
                return "Taker"
            else:
                return "Maker"
        
    def maker_taker_cost_calc(row):
        pair = row['quote_currency'] + "/" + primary_currency
        exchange = row['exchange']
        fee = row['fees_bps']
        total = fee * row['fill_price'] * row['fill_size'] * all_rates.get(pair, 1) / 10000
        return total 

    def xrate_calc(row):
        if new_version:
            try:
                info = json.loads(str(row['info']).replace("'", "\""))
                xrate = info['XRATE']
                xrate = float(xrate)
                return xrate 
            except Exception as e:
                info = float(str(row['info']).split(",")[2].split(":")[1])
                return info 


        else:

            info = str(row['info'])
            info = info.split(',')[1]
            info = info.split(':')[1]
            return float(info)

    def notional_calc(row):
        return row['fill_size'] * row['fill_price'] * row['xrate']
    

    def cost_calc(row):
        current_value = row['synth_price'] * row['fill_size'] * row['fees_bps']/10000
        return current_value

    all_trades = trades.copy(deep=True)
    all_trades['pair'] = all_trades['base_currency'] + '/' + all_trades['quote_currency']
    all_trades["local_price"] = all_trades.apply(get_local_price, axis=1)
    all_trades["pnl_local"] = all_trades.apply(pnl_local_currency_calc, axis=1)
    all_trades["pnl_%s"%primary_currency] = all_trades.apply(pnl_primary_currency_calc, axis=1)
    all_trades['cum_pnl'] = all_trades['pnl_%s'%primary_currency].cumsum()
    all_trades["theo_hedge_cost"] = all_trades.apply(theo_hedgecost_calc, axis=1)
    all_trades["hedge_pnl"] = all_trades.apply(hedge_pnl_calc, axis=1)
    all_trades["level"] = all_trades.apply(get_level, axis=1)
    all_trades["fees_bps"] = all_trades.apply(get_fees, axis=1)
    all_trades["maker/taker"] = all_trades.apply(maket_taker_calc, axis=1)
    all_trades["maker/taker_costs"] = all_trades.apply(maker_taker_cost_calc, axis=1)
    all_trades["xrate"] = all_trades.apply(xrate_calc, axis=1)
    all_trades["notional"] = all_trades.apply(notional_calc, axis=1)
    all_trades["cost"] = all_trades.apply(cost_calc, axis=1)
    

    return all_trades 


##################################################### G sheets related stuff ########################################################################################################################################################


def update_records(gsheets_name, worksheet_name, new_table, date):
    ## First things first, lets read the gsheets and see if records exist 
    
    try:
        sheet = g_sheets.initalize_google_api(gsheets_name)
        try: 
            worksheet = sheet.worksheet(worksheet_name)
        except Exception as e:
            print("Please create a worksheet called: '%s' inside the spreadsheet called: '%s'"%(worksheet_name, gsheets_name))
            print(e)
            return 
    except Exception as e:
        print("Please create a spreadsheet called: '%s' and a worksheet inside that called: '%s' ! Once this is done please share with pytrader@tradingbotdataassistant.iam.gserviceaccount.com"%(gsheets_name, worksheet))
        print(e)
        return 

    table = get_as_dataframe(worksheet, parse_dates=False, header=None, include_index=True,include_column_header=True)
    table.columns = table.iloc[0]
    table = table.reindex(table.index.drop(0))
    dates_populated = []

    try:
        table = table.set_index('Date')
        table = table.dropna()
        dates_populated = list(table.index.values)
        db_exists = True
    except Exception as e:
        print("Empty table! Lets just build our own instead")
        db_exists = False

    #print(table)
    #print("Date given: %s"%date)
    #print("Dates populated: %s"%dates_populated)
        
    # This means we will need to update the table cuz the date does not exists 
 
    if date in dates_populated:
        table = table.drop(date)

    # if it exists we need to combine tables 
    if db_exists:
        final_record_table = pd.concat([table, new_table], axis=0, sort=False)
    # if not we just need to use our current table 
    else:
        final_record_table = new_table
    final_record_table = final_record_table.reindex_axis(sorted(final_record_table.columns), axis=1)
    final_record_table = final_record_table.sort_values(by='Date', axis=0)
    final_record_location = g_sheets.create_and_fill_cell(final_record_table, [1,1], worksheet, 'Xrate')       

    print("Updated records for %s successfully!"%(gsheets_name))
    

def update_trade_record(gsheets_name, worksheet_name, xrates_table, balance, pnl, all_trades, gsheets_offset):

    try:
    # This sheet will always exists
        sheet = g_sheets.initalize_google_api(gsheets_name)
    except Exception as e:
        print("Sheet does not exist! Please create a sheet called '%s' and share with pytrader@tradingbotdataassistant.iam.gserviceaccount.com!!")
        return 
    # First check if worksheet of this date exists, if yes delete it 
    try:
        worksheet = sheet.worksheet(worksheet_name)
        sheet.del_worksheet(worksheet)
    except Exception as e:
        # if it doesnt exist it will excpet so its fine 
        pass 

        
    # Now add a new worksheet for this date 
    try:
        worksheet = sheet.add_worksheet(title=worksheet_name, rows="10000", cols="50")
    except Exception as e:
        print(e)
        return 

    end_point_rates = g_sheets.create_and_fill_cell(xrates_table, [1,1], worksheet, 'Xrate')

    end_point_balance = g_sheets.create_and_fill_cell(balance, [end_point_rates[0]+gsheets_offset, 1], worksheet, 'Balance')

    end_point_pnl = g_sheets.create_and_fill_cell(pnl, [end_point_balance[0]+gsheets_offset,1], worksheet, 'Pnl')

    end_point_trades = g_sheets.create_and_fill_cell(all_trades, [end_point_pnl[0]+gsheets_offset,1], worksheet, 'Trades')

    try:
        worksheet_template = sheet.worksheet('Sheet1')
        sheet.del_worksheet(worksheet_template)
    except Exception as e:
        print(e)

    

############################################################################################Record Balance related #######################################################################################
def get_diff_from_pnl(pnl, date):
    diff = pd.DataFrame(pnl['DIFF'])
    diff = diff.drop("Total")
    diff = diff.transpose()
    all_coins_in_balance = list(diff.columns)
    diff.rename(index={'DIFF':date, 'Coin': 'Date'}, inplace=True)
    diff.columns = [str(col) + '_diff' for col in diff.columns]
    diff.index.name = 'Date'
    return diff 

def get_start_inventory_from_pnl(pnl, date):
    start = pd.DataFrame(pnl['START'])
    start = start.drop("Total")
    start = start.transpose()
    all_coins_in_balance = list(start.columns)
    start.rename(index={'START':date, 'Coin': 'Date'}, inplace=True)
    start.columns = [str(col) + '_start' for col in start.columns]
    start.index.name = 'Date'
    return start 

def get_total_inventory_from_balance(balance, pnl, date):
    #first get all coins 
    start = pd.DataFrame(pnl['START'])
    start = start.drop("Total")
    start = start.transpose()
    all_coins_in_balance = list(start.columns)

    # now do the balance calculations 
    tot_balance =  balance.copy(deep=True)
    tot_balance = tot_balance.set_index("Coin")
    inventory = {}
    for coin in all_coins_in_balance:
        inventory[coin] = tot_balance["TOTAL_INV"][coin]
    final_inventory_table  = pd.DataFrame(pd.Series(inventory,index=inventory.keys()))
    final_inventory_table = final_inventory_table.transpose()
    final_inventory_table.rename(index={0:date}, inplace=True)
    final_inventory_table.columns = [str(col) + '_inventory' for col in final_inventory_table.columns]
    final_inventory_table.index.name = 'Date'
    return final_inventory_table

def get_balance_by_exch(balance, pnl, date):
    # first get all coinns 
    start = pd.DataFrame(pnl['START'])
    start = start.drop("Total")
    start = start.transpose()
    all_coins_in_balance = list(start.columns)

    # now do balance calculations 
    balance_by_exch = balance.copy(deep=True)
    balance_by_exch = balance_by_exch.drop(balance_by_exch[balance_by_exch['Free/Total'] == 'Free'].index)
    balance_by_exch = balance_by_exch[balance_by_exch['Coin'].isin(all_coins_in_balance)]
    balance_by_exch = balance_by_exch.drop('TOTAL_INV', axis=1)
    balance_by_exch = balance_by_exch.drop('Free/Total', axis=1)
    balance_by_exch = balance_by_exch.set_index('Coin')
    balance_by_exch.columns = [str(col) + '_balance' for col in balance_by_exch.columns]
    balance_by_exch.rename(index={'TOTAL':date}, inplace=True)
    balance_by_exch = balance_by_exch.stack().to_frame().T
    balance_by_exch.rename(index={0:date}, inplace=True)
    return balance_by_exch

def build_balance_record(pnl, balance, date):
    diff = get_diff_from_pnl(pnl, date)
    start = get_start_inventory_from_pnl(pnl, date)
    inventory = get_total_inventory_from_balance(balance,pnl, date)
    balance_by_exch = get_balance_by_exch(balance, pnl, date)

    balance_record = pd.concat([diff, start,inventory, balance_by_exch], axis=1, sort=False)
    balance_record.index.name = "Date"
    # Since the column values are tuples, we need to make them into a string 
    balance_record.columns = [':'.join(ele) if type(ele)!=str else ele  for ele in balance_record.columns.values.tolist()]
    return balance_record

################################################################################################## Record Volume ####################################################################################################################################
# get table for volume per exchange 
def get_vol_exch(all_trades, xrates):

    # def get_notional(row):
    #     pair = row.name + "/" + primary_currency
    #     total = row['fill_size'] * xrates.get(pair)
    #     return total 


    def get_perc_vol(row):
        total_volume = vol_exch["notional"].sum()
        return row['notional']*100/total_volume
    


    vol_exch = all_trades[[ 'base_currency', 'notional']].copy()
    vol_exch = vol_exch.groupby(['base_currency']).sum()


    #vol_exch["notional"] = vol_exch.apply(get_notional, axis=1)
    vol_exch["perc_vol"] = vol_exch.apply(get_perc_vol, axis=1)

    sum_row = {col: vol_exch[col].sum() for col in vol_exch}
    # Turn the sums into a DataFrame with one row with an index of 'Total':
    sum_df = pd.DataFrame(sum_row, index=["Total"])
    # Now append the row:
    vol_exch = vol_exch.append(sum_df)
    return vol_exch 


def get_inventory(all_trades, date):
    inventory = all_trades[['base_currency', 'fill_size']].copy()
    inventory = inventory.groupby(['base_currency' ]).sum()
    inventory = inventory.transpose()
    inventory.index = [date]
    inventory.rename(index={'base_currency':'Date'}, inplace=True)
    inventory.columns = ['a_' + str(col) + '_traded' for col in inventory.columns]
    inventory.index.name = 'Date'
    return inventory

def get_notional_volume(all_trades, xrates,  date, primary_currency):
    not_vol = all_trades[['base_currency', 'notional']].copy()
    not_vol = not_vol.groupby(['base_currency' ]).sum()

    # def get_notional(row):
    #     pair = row.name + "/" + primary_currency
    #     total = row['fill_size'] * xrates.get(pair)
    #     return total 

    # not_vol["notional"] = not_vol.apply(get_notional, axis=1)

    # not_vol = not_vol.drop(['fill_size'], axis=1)
    not_vol = not_vol.transpose()
    not_vol.index = [date]
    not_vol.rename(index={'base_currency':'Date'}, inplace=True)
    not_vol.columns = ['b_' + str(col) + '_notional_vol' for col in not_vol.columns]
    not_vol.index.name = 'Date'
    return not_vol 

##TODO do not use exchagne rate!
def get_percentage_volume(all_trades,xrates, date, primary_currency):
    tokens_vol = all_trades[['base_currency', 'notional']].copy()
    tokens_vol = tokens_vol.groupby(['base_currency' ]).sum()

    # ## get notional 
    # def get_notional(row):
    #     pair = row.name + "/" + primary_currency
    #     total = row['fill_size'] * xrates.get(pair)
    #     return total 

    # tokens_vol["notional"] = tokens_vol.apply(get_notional, axis=1)
    
    ## calculate percentages 
    def get_perc_vol(row):
        total_volume =  tokens_vol["notional"].sum()
        return row['notional']*100/total_volume
    
    perc_vol = tokens_vol.copy(deep=True)
    
    perc_vol["perc_vol"] = perc_vol.apply(get_perc_vol, axis=1)
    
    
  #  perc_vol = perc_vol.drop(['fill_size'], axis=1)
    perc_vol = perc_vol.drop(['notional'], axis=1)
    perc_vol = perc_vol.transpose()
    perc_vol.index = [date]
    perc_vol.rename(index={'base_currency':'Date'}, inplace=True)
    perc_vol.columns = ['c_' + str(col) + '_perc_vol' for col in perc_vol.columns]
    perc_vol.index.name = 'Date'
    return perc_vol 


def get_volumes_by_exch(all_trades, xrates, date, primary_currency):
    vol_exch = all_trades[[ 'exchange', 'base_currency', 'notional']].copy()
    vol_exch = vol_exch.groupby(['base_currency', 'exchange']).sum()


    # def get_notional(row):
    #     pair = row.name[0] + "/" + primary_currency
        
    #     total = row['fill_size'] * xrates.get(pair)
    #     return total 


    def get_perc_vol(row):
        total_volume = vol_exch["notional"].sum()
        return row['notional']*100/total_volume
   # vol_exch["notional"] = vol_exch.apply(get_notional, axis=1)
    vol_exch["perc_vol"] = vol_exch.apply(get_perc_vol, axis=1)

    unique_exchanges = set([ele[1] for ele in vol_exch.index.values.tolist()])
    notional_volume_exchange = {}
    notional_perc_vol_exchange = {}

    for exch in unique_exchanges:
        notion_vol = vol_exch.loc[[ele for ele in vol_exch.index.values.tolist() if ele[1]==exch]]['notional'].sum()
        perc_vol = vol_exch.loc[[ele for ele in vol_exch.index.values.tolist() if ele[1]==exch]]['perc_vol'].sum()
        
        notional_volume_exchange[exch] = notion_vol
        notional_perc_vol_exchange[exch] = perc_vol 

    not_vol_list = []
    perc_vol_list = []
    for exch in unique_exchanges:
        not_vol_list.append(notional_volume_exchange.get(exch))
        perc_vol_list.append(notional_perc_vol_exchange.get(exch))

    data = {date: notional_volume_exchange}
    not_vol_exch_table = pd.DataFrame.from_dict(data, orient='index', columns=unique_exchanges)
    not_vol_exch_table.columns = ['d_' + str(col) + '_notional_vol' for col in not_vol_exch_table.columns]
    not_vol_exch_table.index.name = 'Date'


    data = {date: notional_perc_vol_exchange}
    perc_vol_exch_table = pd.DataFrame.from_dict(data, orient='index', columns=unique_exchanges)
    perc_vol_exch_table.columns = ['e_' + str(col) + '_perc_vol' for col in perc_vol_exch_table.columns]
    perc_vol_exch_table.index.name = 'Date'
    return not_vol_exch_table , perc_vol_exch_table



def build_volume_record(all_trades, xrates, date, primary_currency):
    inventory = get_inventory(all_trades, date)
    notional_volume = get_notional_volume(all_trades, xrates,  date, primary_currency)
    percentage_volume = get_percentage_volume(all_trades, xrates, date , primary_currency)
    notional_volume_exch, percentage_volume_exch = get_volumes_by_exch(all_trades, xrates, date , primary_currency)

    volume_record = pd.concat([inventory, notional_volume, percentage_volume,notional_volume_exch,percentage_volume_exch  ], axis=1, sort=False)
    return volume_record , notional_volume_exch, percentage_volume_exch

###############################################################################################################################################################################################################
def get_table_from_dict(data, date):
    keys =list(data.keys())
    info = list(data.values())
    main_data = {date: info}
    table = pd.DataFrame.from_dict(main_data,orient='index', columns=keys )
    table.index.name = 'Date'
    return table 
def get_stats_pnl_record(trades, all_trades, primary_currency, pnl_calculated):
    trade_pnl = trades['pnl_%s'%primary_currency].sum()
    theo_hedge_pnl = trades['hedge_pnl'].sum() 
    costs = trades['cost'].sum()
    dollar_volume = trades['notional'].sum()
    margin = (trade_pnl - costs) /dollar_volume
    return {'trade_pnl': trade_pnl, 'theo_hedge_pnl': theo_hedge_pnl, 'costs': costs, 'dollar_volume': dollar_volume, 'margin': margin}

def build_pnl_record(pnl, all_trades, primary_currency, date, old):
    if old:
        pnl_stats = {"pnl_usdt":  pnl['PandL_%s'%primary_currency]['Total'], 
                  "pnl_change": "=SUM($B$2:B3)",
                    "pnl_calc":(all_trades['pnl_%s'%primary_currency].sum() - all_trades['cost'].sum())

                 ,
                    "pnl_theo":(all_trades['hedge_pnl'].sum() - all_trades['cost'].sum()) 
            }
    else:
         pnl_stats = {"pnl_usdt":  pnl['PL_$']['Total'], 
                  "pnl_change": "=SUM($B$2:B3)",
                    "pnl_calc":(all_trades['pnl_%s'%primary_currency].sum() - all_trades['cost'].sum())

                 ,
                    "pnl_theo":(all_trades['hedge_pnl'].sum() - all_trades['cost'].sum()) 
            }


    pnl_record_table = get_table_from_dict(pnl_stats, date)

    # Now get the total stats 
    total_pnl_record = get_stats_pnl_record(all_trades, all_trades, primary_currency, pnl_stats['pnl_calc'])
    total_pnl_table = get_table_from_dict(total_pnl_record, date)


    trades_dict = get_unique_trades(all_trades)
    unique_bases = list(all_trades['base_currency'].unique())
    pnl_table_dict = {}
    for base in unique_bases:
        stats = get_stats_pnl_record(trades_dict[base], all_trades, primary_currency,pnl_stats['pnl_calc'] )
        record = get_table_from_dict(stats, date)
        pnl_table_dict[base] = record 
    for base in pnl_table_dict:
        pnl_table_dict[base].columns =   [base + '_' + str(col) for col in pnl_table_dict[base].columns] 

    pnl_record_table = pd.concat([pnl_record_table,total_pnl_table ], axis=1, sort=False)
    for base in pnl_table_dict:
        pnl_record_table = pd.concat([pnl_record_table, pnl_table_dict[base]], axis=1, sort=False)
    return pnl_record_table

################################### VOL TABLE ##############################################

def parse_trades_by_vol(trades, xrates, primary_currency):
    mini_trades = trades[['side', 'exchange', 'base_currency', 'fill_size', 'notional']].copy()
            # simple buy table 
    buy_table = mini_trades.loc[mini_trades['side'] == 'BUY']
    # simple sell table 
    sell_table = mini_trades.loc[mini_trades['side'] == 'SELL']
    
    
    
    buy_table = buy_table.groupby(['exchange','base_currency']).sum()
    buy_table = buy_table.rename(columns={'fill_size': 'buy_volume'})
    buy_table = buy_table.rename(columns={'notional': 'notional_buy'})
    
    
    sell_table = sell_table.groupby(['exchange','base_currency']).sum()
    sell_table = sell_table.rename(columns={'fill_size': 'sell_volume'})
    sell_table = sell_table.rename(columns={'notional': 'notional_sell'})
    
    volumes_table = buy_table 
  
    volumes_table['sell_volume'] = sell_table['sell_volume']
    volumes_table['notional_sell'] = sell_table['notional_sell']
    
    volumes_table["sell_volume"] = volumes_table["sell_volume"].fillna(0)
    volumes_table["buy_volume"] = volumes_table["buy_volume"].fillna(0)
    volumes_table["total_volume"] = volumes_table["buy_volume"] + volumes_table["sell_volume"]
    volumes_table["net_volume"] = volumes_table["buy_volume"] - volumes_table["sell_volume"]
   

    volumes_table["notional_total"] = volumes_table["notional_buy"] + volumes_table["notional_sell"]
    volumes_table["perc_buy_volume"] = volumes_table["buy_volume"] *100/  volumes_table["total_volume"].sum()
    volumes_table["perc_sell_volume"] = volumes_table["sell_volume"] *100/ volumes_table["total_volume"].sum()
    volumes_table["perc_total_volume"] = volumes_table["perc_buy_volume"] + volumes_table["perc_sell_volume"]

    curs = set(list(volumes_table.index.get_level_values(1)))
    return volumes_table


def parse_trades_by_algo(trades, xrates, primary_currency):
    mini_trades = trades[['side', 'algoname', 'fill_size', 'base_currency', 'notional']].copy()
    # simple buy table 
    buy_table = mini_trades.loc[mini_trades['side'] == 'BUY']
    # simple sell table 
    sell_table = mini_trades.loc[mini_trades['side'] == 'SELL']
    buy_table = buy_table.groupby(['algoname', 'base_currency']).sum()
    buy_table = buy_table.rename(columns={'fill_size': 'buy_volume'})
    buy_table = buy_table.rename(columns={'notional': 'notional_buy'})
    sell_table = sell_table.groupby(['algoname', 'base_currency']).sum()
    sell_table = sell_table.rename(columns={'fill_size': 'sell_volume'})
    sell_table = sell_table.rename(columns={'notional': 'notional_sell'})
    volumes_table = buy_table 
    volumes_table['sell_volume'] = sell_table['sell_volume']
    volumes_table['notional_sell']  = sell_table['notional_sell']
    volumes_table["sell_volume"] = volumes_table["sell_volume"].fillna(0)
    volumes_table["buy_volume"] = volumes_table["buy_volume"].fillna(0)
    volumes_table["total_volume"] = volumes_table["buy_volume"] + volumes_table["sell_volume"]
    volumes_table["net_volume"] = volumes_table["buy_volume"] - volumes_table["sell_volume"]

  
    volumes_table["notional_total"] = volumes_table["notional_buy"] + volumes_table["notional_sell"]
    
    return volumes_table
def build_volume_stats(trade_cur, primary_currency, algo_intermediary_table,volume_intermediary_table ):
    abx = volume_intermediary_table.reset_index()
    stat_dict = {'vol_tokens':abx[abx["base_currency"]==trade_cur].sum()['total_volume'], 
                'vol_%s'%(primary_currency):abx[abx["base_currency"]==trade_cur].sum()['notional_total'],
                }

    sub_tab = abx[abx["base_currency"] == trade_cur]

    for index, row in sub_tab.iterrows():
        notional_buy = row['notional_buy']
        notional_sell = row['notional_sell']
        total = row['notional_total']
        perc = row['perc_total_volume']
        exchange = row['exchange']

        stat_dict['%s_%s'%(exchange, 'notional_buy')] = notional_buy
        stat_dict['%s_%s'%(exchange, 'notional_sell')] = notional_sell
        stat_dict['%s_%s'%(exchange, 'notional_total')] = total
        try:
            stat_dict['%s_%s'%(exchange, 'perc_total_volume')] = (total * 100 )/ (stat_dict['vol_%s'%primary_currency]) 
        except Exception as e:
            stat_dict['%s_%s'%(exchange, 'perc_total_volume')] = 0

    sub_tab = algo_intermediary_table.reset_index()
    sub_tab = sub_tab[sub_tab["base_currency"] == trade_cur]

    for index, row in sub_tab.iterrows():
        notional_buy = row['notional_buy']
        notional_sell = row['notional_sell']
        total = row['notional_total'] 
        perc = 0 
        algo = row['algoname']

        stat_dict['z_%s_%s'%(algo, 'notional_buy')] = notional_buy
        stat_dict['z_%s_%s'%(algo, 'notional_sell')] = notional_sell
        stat_dict['z_%s_%s'%(algo, 'notional_total')] = total
        try:
            stat_dict['z_%s_%s'%(algo, 'perc_total_volume')] = (total * 100)/ stat_dict['vol_%s'%primary_currency] 
        except Exception as e:
            stat_dict['%s_%s'%(exchange, 'perc_total_volume')] = 0
    return stat_dict

def get_vol_stats(trades, xrates, primary_currency):
    volume_intermediary_table = parse_trades_by_vol(trades, xrates, primary_currency)
    algo_intermediary_table = parse_trades_by_algo(trades, xrates, primary_currency)
    unique_bases = list(trades['base_currency'].unique())
    all_stats = {}
    for trade_cur in unique_bases:
        all_stats[trade_cur] = build_volume_stats(trade_cur, primary_currency, algo_intermediary_table, volume_intermediary_table)

    return all_stats, volume_intermediary_table, algo_intermediary_table

def build_vol_reports(vol_stats, date):
    vol_reports = {}

    for cur in vol_stats:
        vol_reports[cur] = get_table_from_dict(vol_stats[cur], date)
    return vol_reports 
